TypoClock - Design Clock
==================================================

A simply clock, with a great design in CSS3 and Jquery.

Turn your computer into an original clock. This project is entirely inspired clocks QLOCKTWO. It is a tribute to the fabulous Biegert & Funk clocks. Thank you to Bertrand, who introduced me to these clocks.


[Live Demo](https://typoclock.eu/ "Live Demo")

[![Typoclock website](https://typoclock.eu/images/typoclock-facebook-cover.png "Typoclock website")](https://typoclock.eu)